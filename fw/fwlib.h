/*
 * RetroAdvance framework for GBA - C Header
 *
 * (C) Copyright 2021 Pedro Gimeno Fortea
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

typedef  uint8_t  u8;
typedef   int8_t  s8;
typedef uint16_t u16;
typedef  int16_t s16;
typedef uint32_t u32;
typedef  int32_t s32;
typedef uint64_t u64;
typedef  int64_t s64;
typedef unsigned uint;

u32 fw__getMap(int x, int y);
void fw__setMap(int x, int y, u32 id);

void fw__intHandler(void);
void fw__waitVSyncInt(void);
void fw__copySpritesReverse(const void *src, size_t n);
void *fw__memcpy32_small(void *restrict d, const void *restrict s, size_t n);
u32  fw__reusePalette(void *palette, u32 n);
void fw__chgDrawPaletteColour(u8 dest, s32 src);
void fw__chgScrPaletteColour(u8 dest, s32 src);
void fw__chgNextPaletteColour(u8 dest, s32 src);
void fw__resetPalette(void);
void fw__camera(int x, int y);
void fw__drawMap(int x, int y, u32 tx, u32 ty, u32 tw, u32 th);
void fw__drawSprite(int x, int y, u16 tile, u8 width, u8 height, bool flipX, bool flipY);
void fw__renderMap(u32 tx, u32 ty, u32 tw, u32 th, int x, int y, u16 *bgmap,
  u16 *bgofs, u32 pal);
void fw__clear(uint32_t n);
void fw__present(void);
bool fw__secondVSync(void);
void fw__init(void);
