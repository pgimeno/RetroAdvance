--[[

 RetroAdvance framework - TGA to Binary Conversion utility

 (C) Copyright 2021 Pedro Gimeno Fortea

 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.

 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

--]]

-- Split a Truevision Targa .TGA file into bitmap and palette
local f = io.open(arg[1], "rb")
if not f then
  error(string.format("Can't open input file %q", arg[1]))
end

do
  local hdr = f:read(18)
  local
    L, cmt, dtc, cmo, cml, cmd, imx, imy, imw, imh, imbpp, imd
    L, cmt, dtc, cmo, cml, cmd, imx, imy, imw, imh, imbpp, imd
      = string.unpack("<BBBI2I2BI2I2I2I2BB", hdr)

  if cmt ~= 1 or dtc ~= 1 then
    error("Not an indexed image")
  end
  if cmo ~= 0 or cml ~= 16 or cmd ~= 24 then
    error("Unsupported palette origin, count or bit depth")
  end
  if imx ~= 0-- or imy ~= 0  -- gimp writes imy=height???
  then
    error("Image not at origin")
  end
  -- don't check image size when saving palette
  if arg[2] ~= "-p" and (imw ~= 256 or imh ~= 128) then
    error("Image not 256x128")
  end
  if imbpp ~= 8 then
    error("Image not 8bpp")
  end
  if imd == 0 then
    error("Only top-down TGAs supported")
  elseif imd ~= 0x20 then
    error("Unknown image descriptor byte")
  end

  if L ~= 0 then
    f:read(L)
  end
end
local palette = f:read(16*3)
local imdata = f:read(256*128)

local argi = 2
while argi <= #arg do
  if arg[argi] == "-p" and arg[argi+1] then
    argi = argi + 1
    local outf = io.open(arg[argi], "wb")
    for i = 1, 16*3, 3 do
      local b, g, r = palette:byte(i, i+2)
      if b % 8 ~= 0 or g % 8 ~= 0 or r % 8 ~= 0 then
        io.stderr:write("Warning: bits 0-2 of a colour component are not zero\n")
        b = b - b % 8
        g = g - g % 8
        r = r - r % 8
      end
      local word = b/8*2^10+g/8*2^5+r/8
      outf:write(string.pack("<I2", word))
    end
    outf:close()
  elseif arg[argi] == "-d" and arg[argi+1] then
    argi = argi + 1
    local warn511 = false
    local outf = io.open(arg[argi], "wb")
    for y = 0, 15 do
      for x = 0, 31 do
        for ty = 0, 7 do
          for tx = 0, 7, 2 do
            -- x, y: map coordinates; tx, ty: subtile coordinates
            local ofs = y*256*8 + ty*256 + x * 8 + tx
            local a, b = imdata:byte(ofs+1, ofs+2)
            if a > 15 or b > 15 then
              io.stderr:write("Warning: nonzero high bits in TGA image data\n")
            end
            local c = (a % 16) + 16 * (b % 16)
            outf:write(string.char(c))
            if x == 31 and y == 15 and c ~= 0 and not warn511 then
              warn511 = true
              io.stderr:write("Warning: tile 511 (31,15) has nonzero pixels\n")
            end
          end
        end
      end
    end
    outf:close()
  else
    io.stderr:write("Unrecognized option; usage:"
.."\n  lua tga2bin.lua <inputfile.tga> [-p <palette.bin>] [-d <imagedata.bin>]")
  end
  argi = argi + 1
end
