--[[

 RetroAdvance framework - Tiled Lua Map to Binary converter

 (C) Copyright 2021 Pedro Gimeno Fortea

 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.

 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

--]]

local pack, open = string.pack, io.open
local infn, outfn = 1, 2
if arg[1] == '-s' then infn, outfn = 2, 3 end
local map = dofile(arg[infn])
local outf = open(arg[outfn], "wb")
if arg[1] == '-s' then
  outf:write(pack("<i4i4", map.width, map.height))
else
  local data = map.layers[1].data
  for i = 1, #data do
    outf:write(pack("<i2", data[i] == 0 and 511 or data[i] - 1))
  end
end
outf:close()
