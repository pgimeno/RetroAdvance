#
# Makefile for GBA projects using the RetroAdvance Framework
#
# (C) Copyright 2021 Pedro Gimeno Fortea
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
# IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

ifeq ($(PRJ),)
  $(error PRJ must be defined when invoking make)
endif

ACSL_PATH := ../acsl/
ARM := arm-none-eabi-
OBJCOPY := $(ARM)objcopy
CC := $(ARM)gcc
LD := $(ARM)ld
AR := $(ARM)gcc-ar
AS := $(ARM)as
ifeq ($(OS),Windows_NT)
  DIRSEP := $(BACKSPACE)
else
  DIRSEP := /
endif
NELUA_PATH := ..$(DIRSEP)nelua-lang$(DIRSEP)
LUA := $(NELUA_PATH)nelua-lua
NELUA := $(NELUA_PATH)nelua

# User directory
USER := projects/$(PRJ)

QPRJ := $(subst $() ,\ ,$(PRJ))
QUSER := $(subst $() ,\ ,$(USER))

NELUA_SEARCH_PATH := $(USER)/?.nelua

# Output directory with system-dependent separator
DOUT := projects$(DIRSEP)$(PRJ)$(DIRSEP)out
# Quoted output directory
QOUT := $(QUSER)/out
ifeq ($(OS),Windows_NT)
  MKDIR_OUT := if not exist "$(DOUT)" mkdir "$(DOUT)"
  MKDIR_OBJ := if not exist "$(DOUT)" mkdir "$(DOUT)"
  RMDIR := rd /s /q
else
  MKDIR_OUT := mkdir -p "$(DOUT)"
  MKDIR_OBJ := mkdir -p obj
  RMDIR := rm -rf
endif

B_CFLAGS := -O3 -march=armv4t -mthumb -nostdinc -I$(ACSL_PATH)include\
 -ffast-math -fomit-frame-pointer -ffunction-sections -Ifw $(CFLAGS)
B_LDFLAGS := --specs="$(ACSL_PATH)"gba.specs -L"$(ACSL_PATH)"\
 -T"$(ACSL_PATH)"gba.ld -Wl,--gc-sections $(LDFLAGS)
B_ASFLAGS := -I "$(DOUT)" -I fw $(ASFLAGS)

all: $(QOUT)/$(QPRJ).gba

$(QOUT)/framework.o: fw/framework.nelua $(QUSER)/main.nelua | $(QOUT)
	$(NELUA_PATH)nelua --cc "$(CC)" --cflags="$(B_CFLAGS) $(B_LDFLAGS)\
 -c" --cache-dir "$(DOUT)" -C -b --path "$(NELUA_SEARCH_PATH)" -o "$@" "$<"

$(QOUT)/%.o: fw/%.S | $(QOUT)
	$(AS) $(B_ASFLAGS) -o "$@" "$<"

obj/asmlib.o: fw/asmlib.s | obj
	$(AS) $(B_ASFLAGS) -o "$@" "$<"

$(QOUT)/%.o: fw/%.c
	$(CC) $(B_CFLAGS) -c -o "$@" $^

obj/fwlib.o: fw/fwlib.c fw/fwlib.h | obj
	$(CC) $(B_CFLAGS) -c -o "$@" $<

$(QOUT)/$(QPRJ).elf: $(QOUT)/bindata.o obj/asmlib.o obj/fwlib.o\
 $(QOUT)/framework.o
	$(CC) $(B_LDFLAGS) -o "$@" "$(DOUT)/bindata.o" obj/asmlib.o\
 obj/fwlib.o "$(DOUT)/framework.o"

$(QOUT)/palette2.bin: fwassets/palette2.tga | $(QOUT)
	$(LUA) fw/tga2bin.lua "$<" -p "$@"

$(QOUT)/bindata.o: fw/bindata.s $(QOUT)/palette2.bin $(QOUT)/palette.bin\
 $(QOUT)/tileset.bin $(QOUT)/spritesheet.bin $(QOUT)/tilemap.bin\
 $(QOUT)/mapsize.bin
	$(AS) $(B_ASFLAGS) -o "$@" $<

$(QOUT)/palette.bin: $(QUSER)/tileset.tga | $(QOUT)
	$(LUA) fw/tga2bin.lua "$<" -p "$@"

$(QOUT)/tileset.bin: $(QUSER)/tileset.tga | $(QOUT)
	$(LUA) fw/tga2bin.lua "$<" -d "$@"

$(QOUT)/spritesheet.bin: $(QUSER)/spritesheet.tga | $(QOUT)
	$(LUA) fw/tga2bin.lua "$<" -d "$@"

$(QOUT)/tilemap.bin: $(QUSER)/tilemap.lua
	$(LUA) fw/map2bin.lua "$<" "$@"

$(QOUT)/mapsize.bin: $(QUSER)/tilemap.lua
	$(LUA) fw/map2bin.lua -s "$<" "$@"

$(QOUT)/$(QPRJ).gba: $(QOUT)/$(QPRJ).elf
	$(OBJCOPY) -O binary "$<" "$@"
	$(LUA) fw/patch-cksum.lua "$@"

$(QOUT):
	$(MKDIR_OUT)

obj:
	$(MKDIR_OBJ)

clean-prj:
	$(RMDIR) "$(DOUT)"

clean-fw:
	$(RMDIR) obj

clean: clean-prj clean-fw

#.PRECIOUS: $(QOUT)/%.elf

.PHONY: all clean clean-prj clean-fw
